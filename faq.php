<?php
include ("header.php");
menu_select("faq");
include("searchjs.php");
?>
</div>

<div id="content"><div id="info"><h2>FAQ</h2></div>
<div id="page">
<script type="text/javascript">
    function installSearchEngine() {
        if (window.external && ("AddSearchProvider" in window.external))
            window.external.AddSearchProvider("http://www.tonlesapdict.com/opensearch.xml.php");
        else
            alert("Your browser does not support OpenSearch search engine plugins");
    }
</script>
<ol>
<li><a href="#cantfindword">Why doesn't this dictionary contain ...?</a></li>
<li><a href="#howaccurate">How accurate is this dictionary?</a></li>
</ol>
<h3>Answers</h3>
<ol>
<li>
    <p><a name="cantfindword" /><b>Why doesn't this dictionary contain ...?</b><br/>
    There are many reasons why you might not be able to find the word you are looking for. Firstly, the dictionary might be missing the word. We try to spot missing words by looking at the logs of what people have searched for, and then add them if appropriate. Secondly you might be searching for a conjugated form of a verb rather than the general form. Try stripping off prefixes and suffixes.</p>
</li>
<li>
    <p><a name="howaccurate" /><b>How accurate is this dictionary?</b><br/>
    All dictionaries have mistakes and this dictionary is no exception. This dictionary is also very much still a work in progress. Some entries have been copied from older dictionaries and have not yet been verified. If you find something that you think is incorrect, please <a href="feedback.php">tell us</a>.</p>
</li>
</ol>
</div>			</div><!-- End of content -->
</div><!-- End of wrapper for sticky footer -->
<?php
include ("footer.php");