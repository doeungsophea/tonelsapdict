#!/bin/sh
#
# This script configures WordPress file permissions based on recommendations
# from http://codex.wordpress.org/Hardening_WordPress#File_permissions
#
# Author: Wil Brown
#
# @uses /bin/sh scriptname.sh path
# @params text path to your WordPress folder from the www root e.g. / or /WordPress
#
ROOT=/var/www/khmernews #your site's root directory
 
if [ $# = 1 ];
then
WP_ROOT=$ROOT$1;
 
#reset to safe defaults
find ${WP_ROOT} -type d -exec chmod 755 {} \;
find ${WP_ROOT} -type f -exec chmod 644 {} \;
 
#secure WordPress configuration file
chmod 440 ${WP_ROOT}/wp-config.php
 
#secure .htaccess
chmod 444 ${WP_ROOT}/.htaccess
 
echo "Done: Reset permissions in folder $WP_ROOT";
else
echo "You need to provide the WordPress directory relative to the root $ROOT";
fi
