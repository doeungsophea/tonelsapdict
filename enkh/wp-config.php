<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tonlesap_dictionary');

/** MySQL database username */
define('DB_USER', 'tonlesap_sophea');

/** MySQL database password */
define('DB_PASSWORD', 'i8EWQ()4]w$A');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
/*
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');
*/
define('AUTH_KEY',         '+f(cXe`5TO0S-(,N M;y5K]^iA-*p-uCh>Lp{&M_B|SQxaE[&{VBF5FxiF|tR&Z*');
define('SECURE_AUTH_KEY',  '/`t2ghnK&w$#;cTv6X-`Dz9s<|&--O0i?AG; un3lXG/H|d@pc0w}Gvp/ BPg_<^');
define('LOGGED_IN_KEY',    '}ch:FDd <TrZZpRD`/uE<J4LC9?EAN` a6p#/kJ-VGMo{Lh/O$$6h9r]i>X-lUB9');
define('NONCE_KEY',        'DgW&)3b9<6?A^l#`PW&>(@|/G0/wQs(I0mo#ReG: |JoZ{JL[(+G@um|>_=XW+|9');
define('AUTH_SALT',        'k>$Y@<0.:Op5(3d{|^}kt@j/`q5|bp3T!hP5-_,l`P. K:_kP#n{xTeVo-]Owj|8');
define('SECURE_AUTH_SALT', ']pb9zv4T|*{^gFrLDg,s!|IXY%cM^cvfw=HLoAv}~m],/USh?ht5L~Fd*UJ=)@c|');
define('LOGGED_IN_SALT',   'C~zA0`u>eHU2kMo]J/BS#O#6$,zi7_S+:ex rE2hGO I<8oZ>hP1wd>|AQwA4T$n');
define('NONCE_SALT',       'r_47}/i}P[vy-L*dmQ7hu3H}t;5(sS]wir20i*M9iW^Z*I7-{H%-v7)AX[$oDLsu');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
