<?php
require_once "functions.php";
$share_term = "Dictionary";
$share_def  = "";
$meta_title = "Tonlesap Dictionary: Meanings & Definitions";
$meta_Description = 'This is an English - Khmer dictionary created to help people who are learning English language. You can search for singular forms and plural forms of nouns, and present tense or past tense forms of verbs.';
if ($_SESSION["is_random"] === false) {
  $share_term = $_SESSION["term"];
  $share_def  = $_SESSION["def_raw"];
  $meta_title = "$share_term Meaning in the Tonlesap Dictionary";
}else{
  $share_term = $meta_title;
  $share_def = $meta_Description;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $meta_title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Description" lang="en" content="<?php echo $meta_Description; ?>">
    <meta name="keywords" content="khmer dictionary, khmer dict, khmer, dictionary, thesaurus, translation, language, words, medical, pronunciation, talking dictionary, dictionaries, word of the day, trending words, word blog, word games, English, free online dictionary, online dictionary, English dictionary, SCRABBLE, words with friends, dict, diction, dictonary, glossary, define, definitions, meaning, word of the day, spelling, pronunciation, dictio, dictiona, dic, dicti, dictionar">
    <meta name="author" content="Meanith Pol">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="<?php echo ":: $share_term ::";?>" />
    <meta property="og:description" content="<?php echo "$share_def";?>" />
    <meta property="og:image" content="http://www.tonlesapdict.com/assets/logo-og70.png" />
    <meta property="og:url" content="<?php echo get_address();?>" />
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">

    <link rel="icon" href="assets/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/public/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/public/css/style.css" type="text/css">

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui-1.8.10.custom.min.js"></script>
    <script type="text/javascript" src="assets/js/custom.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-23607345-3', 'auto');
      ga('send', 'pageview');

    </script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-5582239173060021",
        enable_page_level_ads: true
      });
    </script>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=1397224373918710";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="header">
  <div class="container">
    <div class="nav-bar col-sm-5 col-md-4">
        <a href="http://www.tonlesapdict.com"><img src="assets/logo-og.png"/></a>
        <span class="text-title" style="">Tonlesap <br>Dictionary <br>Online</span>
    </div>
