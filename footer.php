    <div id="footer" class="footer">
        <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <img src="assets/email.png" alt="email" width="16" height="16" style="vertical-align: middle">&nbsp;&nbsp;
                <a href="http://www.tonlesapdict.com/feedback">Feedback</a>
                <!-- &nbsp;&nbsp; | &nbsp;&nbsp;
                <img src="assets/android.png" alt="android" width="16" height="16" style="vertical-align: middle">&nbsp;
                <a href="#">Android app</a>
                &nbsp;&nbsp; | &nbsp;&nbsp;
                <img src="assets/support.png" alt="support" width="16" height="16" style="vertical-align: middle">&nbsp;
                <a href="#">Support</a>	 -->
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <i>Powered by</i> &nbsp;&nbsp;<a href="http://www.tonlesapdict.com">Tonlesap Dictionary</a>
            </div>
        </div>
    </div>
</body>

</html>
