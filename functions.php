<?php
define("DOMAIN","http://www.tonlesapdict.com");

function get_address() {
    $protocol = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
    return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

function menu_select($menu) {
    $domain = DOMAIN;
    $active = array("index"=> "", "faq"=> "", "about"=> "", "feedback"=> "");
    $active[$menu] = "class=\"active\"";
    $str = <<<EOD
            <ul id="mainmenu">
<!--                <li><a href="{$domain}" {$active["index"]}>Dictionary</a></li>
                <li><a href="{$domain}/faq" {$active["faq"]}>FAQ</a></li>
                <li><a href="{$domain}/about" {$active["about"]}>About</a></li>
                <li><a href="{$domain}/feedback" {$active["feedback"]}>Feedback</a></li>
-->
            </ul>
EOD;
    echo $str;
}

function replace_content($content)
{       // middle
    $x   = array( "adj."  , " adj.", "adv." , " adv.",
                      " Coll.", "conj.",
                      "Gen."  ,
                      " Id."  ,
                      " Lit." ,
                      " n."   , "\r\nn. " ,
                      " obj." ,
              " pt."  , "pt."   , " pp." , "prep.", "pron.",
                      " Rur." ,
                      " Sl."  , "Syn."  ,
                      " Urb." ,
                      " v."   , "\r\nv.", "vi."   , " vi." , "vt."  , " vt." );
        // leading/beginning
        $y   = array( "n.", "Id.", "prep.", "v." );

    $str = trim($content);
        //$str = str_replace("\r\n", "", $str);
    foreach ($x as $data) {
        $str = str_replace($data, format_pos($data), $str);
            //$str = preg_replace("/\b$data\b/", format_pos($data), $str);
        }

        foreach ($y as $data) {
            if (startsWith($str, $data) === true) {
            $str = str_replace($data, format_pos($data), $str);
            }
    }
        $str = format_phonetic( format_tild( nl2br($str) ) );
    return $str;
}

function format_pos($pos) {
   return "<span class=\"pos\">$pos</span>";
}

function format_tild($str) {
    //return str_replace('~', '<span class="sym">~</span>', $str);
    return str_replace('~', '<strong>~</strong>', $str);
}

function format_phonetic($str) {
    $result = str_replace('[', '<span class="phon">[', $str );
    $result = str_replace(']', ']</span>', $result );
    return $result;
}

// find word in defintion to generate link
function clean_none_english($word) {
    //$word = preg_replace("/[^a-zA-Z0-9!@#$ \"\\'\\/()\\.,]/", "", $word);
    $word   = preg_replace("/[^a-zA-Z!@#$ \"\\'\\/]/", "", $word);  // also remove number, ~ sign
    $word   = preg_replace("/\\//", " ", $word);  // replace / with space

    // remove strong word coz it is tag name
    $word   = preg_replace("<strong>", "", $word);
    $word   = preg_replace("</strong>", "", $word);
    // remove word 'class' coz confict with <a class="en"
    $word   = str_replace("class", "", $word);
    $word   = str_replace("prep", "", $word);  // prep.

//    $word   = preg_replace("prep.", "", $word);
    //$word   = preg_replace("/^.{2}$/", "", $word);
    $values = explode(" ", $word);
    $length = count($values);

    for ($i=0; $i < $length; $i++) {
        if (strlen($values[$i])<=2) { // 1 & 2 char
            $values[$i] = "";
        }
    }
    $values = array_unique( array_filter($values) );

    return "\"". implode("\", \"", $values). "\"";
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

function db_connect() {
    $mysql = new mysqli('localhost', 'tonlesap_sophea', 'i8EWQ()4]w$A', 'tonlesap_dictionary');
    $mysql->set_charset("utf8");
    return $mysql;
}

function is_exist($word_list) {
    $mysqli = db_connect();
    $word   = $mysqli->escape_string($word_list);
    $exists = array();
    $query  = "SELECT post_title as term FROM dictionary WHERE post_title in (". $word_list .") ORDER BY post_title";
    if ($result = $mysqli->query($query)) {
        while ($row = $result->fetch_array()) {
            $exists[] = $row["term"];
        }
    }
    return $exists;
}

function add_link_definition($definition) {
    // add link to definition
    $original  = $definition;
    $str       = clean_none_english($definition);
    //echo "<!-- $str -->";
    $word_link = is_exist($str);
    $word_link = array_filter($word_link); // remove empty array
    foreach ($word_link as $val) {
        $original = preg_replace("/\b$val\b/", "<a class=\"en\" href=\"".DOMAIN."/$val\">$val</a>", $original);
    }
    //echo $original;
    return $original;
}

function find_word($search_word) {
    $mysqli = db_connect();
    $word  = $mysqli->escape_string($search_word);
    $query = "SELECT post_title as term, post_content as definition FROM dictionary WHERE post_url = '". $word ."' ORDER BY post_title LIMIT 1";
    $end_result = '';
    $term       = "";
    $definition = "";
    if ($result = $mysqli->query($query)) {
        while ($row = $result->fetch_array()) {
            $term       = $row["term"];
            $definition = $row["definition"];
        }
    }
    $def_raw    = mb_substr(strip_tags($definition), 0, 120);  // return only 100 char unicode
    $inner_word = strip_tags($definition, '<strong><em>');  // allow <strong> tag (not remove)
    //$definition = replace_content($definition);
    $definition = add_link_definition($inner_word);

    // $result->free();
    $mysqli->close();
    $definition = replace_content($definition);
    return array("term"=> $term, "def"=> $definition, "def_raw" => $def_raw);
}

function find_suggest($search_word) {
    $mysqli = db_connect();
    $word  = $mysqli->escape_string($search_word);
    $query = "SELECT post_title as term, post_url as url FROM dictionary WHERE post_title like '". $word ."%' ORDER BY post_title LIMIT 15";
    $end_result = '';
    $suggest = array();
    if ($result = $mysqli->query($query)) {
        while ($row = $result->fetch_array()) {
            $term      = $row["term"];
            $url       = $row["url"];
            $suggest[] = array("lang" => "en", "label" => $term, "value" => $url);
        }
    }
    $result->free();
    $mysqli->close();
    return json_encode($suggest);
}

function find_random() {
    $mysqli = db_connect();
    $word   = $mysqli->escape_string($search_word);
    $query  = "SELECT post_url FROM dictionary ORDER BY rand() LIMIT 1";
    $term   = "";
    if ($result = $mysqli->query($query)) {
        while ($row = $result->fetch_array()) {
            $term       = $row["post_url"];
        }
    }
    return $term;
}
?>
