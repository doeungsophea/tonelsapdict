<div id="searchbar" class="col-xs-12 col-sm-7 col-md-8">
    <div><!-- style="float:right" -->
        <script type="text/javascript">
        /* <![CDATA[ */
        $(function() {
            $("#query").autocomplete({
                source: "suggest.php",
                minLength: 2,
                select: function(event, ui) {
                    //console.log(ui.item.value); //return;
                    $('#query').val(ui.item.value);
                    event.preventDefault();
                    $('#searchForm').attr('action', '/' + ui.item.value).submit();
                    //$('#searchForm').submit();
                }
            }).data('autocomplete')._renderItem = function(ul, item) {
                element = $('<li></li>').data('item.autocomplete', item).append('<a class="item-' + item.lang + '">' + item.label + '</a>').appendTo(ul);
                return element;
            };

            $("#searchbtn").bind("click", function(e) {
    		e.preventDefault();
    		$('#searchForm').attr('action', '/' + $('#query').val()).submit();
	    });
        });
        /* ]]> */
        </script>
        <form id="searchForm" method="post" action="<?php echo DOMAIN;?>/">
            <input id="query" name="q" type="text" value="" size="31" maxlength="31" />
            <input type="submit" id="searchbtn" value="" />
        </form>
    </div>
</div>
