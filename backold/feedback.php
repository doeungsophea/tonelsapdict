<?php
include("header.php");
menu_select("feedback");
include("searchjs.php");
?>
</div>
<div id="content">
	<div id="info"><h2>Feedback</h2></div>
	<div id="page"><p><i>Tonlesap Dictionary</i> is a work in progress so it's really helpful to hear from users about how this dictionary can be improved. Please use the form below to send comments about the site or make suggestions for words that are missing from the dictionary.</p>
	<script type="text/javascript">
	function checkFields() {
		var valid = $('#feedback_name').val() != '' && $('#feedback_email').val() != '' && $('#feedback_message').val() != '';
		if (!valid)
			alert("Please complete all fields");
		return valid;
	}
	</script>
	<form method="post" onsubmit="return checkFields();">
	<table align="center" cellspacing="5" cellpadding="0" border="0">
	    <tr>
	        <td width="100">Name</td>
	        <td width="200"><input type="text" id="feedback_name" name="feedback_name" style="width: 190px" value="" /></td>
	        <td width="100" align="center">Email</td>
	        <td width="200"><input type="text" id="feedback_email" name="feedback_email" style="width: 190px" value="" /></td>
	    </tr>
	    <tr>
	        <td width="100" valign="top">Comment</td>
	        <td colspan="3"><textarea rows="3" id="feedback_message" name="feedback_message" style="width: 500px"></textarea></td>
	    </tr>
	    <tr>
	        <td colspan="4" align="center"><input type="submit" value="Send" /></td>
	    </tr>
	</table>
	</form>
</div>
</div><!-- End of content -->
</div><!-- End of wrapper for sticky footer -->
<?php
include("footer.php");