<?php
include ("header.php");
menu_select("about");
include("searchjs.php");
?>
</div>
<div id="content">
	<div id="info"><h2>About</h2></div>
	<div id="page">
		<p>This is an English - Khmer dictionary created to help people who are learning English language. You can search for singular forms and plural forms of nouns, and present tense or past tense forms of verbs.</p>
		<h2>Copyright</h2>
		<p>The dictionary content and reference materials are released under copyright which does not permits you to share, modify, reproduce it in any form.</p>
		<h2>Credits</h2>
		<p>This project is lead by a team, a software developer living in Phnom Penh.
		Significant contributions have been made by <i>Mr. MNP</i>. Other people who have contributed are:
		<ul>
		<li>Mr. ABC</li>
		<li>Mr. XYZ</li>
		</ul></p>
		<p>Some of the content has been derived from the following sources:</p>
		<ul>
		<li>English-Khmer Dictionary by <i>Mr. MNP</i>.</li>
		</ul>
		<h2>Special thanks</h2>
		<ul>
		<li>To the project team for the provision of internet modems for volunteers contributing to the dictionary.</li>
		</ul>
	</div>
</div><!-- End of content -->
</div><!-- End of wrapper for sticky footer -->
<?php
include ("footer.php");