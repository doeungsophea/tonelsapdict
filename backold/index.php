<?php
session_start();
require_once "functions.php";
        
        $search = trim($_GET["q"], "/");
        $is_random = false;
        if (!isset($search))  $search = $_POST["q"];
        // redirect url with space to hyphen
        if (strpos($search, " ") !== false) {
           $search = str_replace(" ", "-", $search);
           header("Location: ". DOMAIN ."/$search");
	} 
	if (isset($search)) {
                if ($search == "") {
                   $search = find_random();
                   $is_random = true;
                }
		$data = find_word($search); 
        } 
        $_SESSION["is_random"] = $is_random;
        $_SESSION["term"     ] = $data["term"];
        $_SESSION["def_raw"  ] = $data["def_raw"];

include "header.php";
menu_select("index");
include "searchjs.php";

?>
</div>
<div id="content" class="content">
	<div id="page" class="main">
	    <ul id="results">
                <?php if (is_random == false) {?>
		<li class="entery">
<div class="fb-like" data-href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
		</li>
                <?php } ?>
	        <li class="entry">
                        <?php if ($is_random == true)  echo "<div>A random word (<a href=\"http://www.tonlesapdict.com\">refresh</a> to get a new one)</div>"; ?>
	        	<span class="headword"><?php echo $data["term"]; ?></span>
	        	<img src="assets/bullet_arrow_right.png"  alt="bullet_arrow_right" class="arrow" /> 
		        <?php echo $data["def"]; ?>
	        </li>
                <li class="entry">
                        <div align="center"><div class="fb-comments" data-href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" data-numposts="5"></div></div>
                </li>
	    </ul>
	</div>
</div><!-- End of content -->
    <div align="center">
<div class="fb-page" data-href="https://www.facebook.com/tonlesapdict" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/tonlesapdict"><a href="https://www.facebook.com/tonlesapdict">Tonlesap Dictionary</a></blockquote></div></div>
    </div>
</div><!-- End of wrapper for sticky footer -->
<?php
include("footer.php");
